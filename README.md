# OCR

This project tries to implement an OCR with pytorch.

## Install dependencies

To install the dependencies either run

```console
$ poetry install
```

Or

```console
$ pip install .
```

## Data source

Currently the text is generated randomly using `random.choice` from a pool of
selected characters and then it is printed into an image using `pillow`.

## Train model

Currently the model is saved in the current working directory with the name `ocr.pt`.
If this file exists it's weights are loaded, trained on new data, each time
a new minimum loss is found the file will be **overwritten**, so if you don't
want to lose the current weights make sure to backup the file.

To train the model make sure to activate the environment and
[install the dependencies](#install-dependencies).
Then run

```console
$ python -m ocr.train
```

## Predict

To run predictions you need to have the file `ocr.pt` in the current working directory.
The image file is read from `stdin`.
To run the OCR in an image run

```console
$ python -m ocr.inference < /path/to/image.png
```
