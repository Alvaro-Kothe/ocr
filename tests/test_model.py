from unittest.mock import patch

import pytest
import torch

from ocr import model


@pytest.fixture
def cnn_layer() -> model.ConvolutionLayer:
    return model.ConvolutionLayer()


@pytest.mark.parametrize(
    ("input_shape", "expected_shape"),
    (
        (
            (10, 1, 32, 32),
            10,
        ),
        (
            (1, 1, 100, 100),
            1,
        ),
    ),
)
def test_convolution_forward(
    input_shape: tuple, expected_shape: tuple, cnn_layer: model.ConvolutionLayer
) -> None:
    input_tensor = torch.rand(*input_shape)
    output = cnn_layer(input_tensor)
    assert output.shape[0] == expected_shape


def test_encoder_forward() -> None:
    convolution_result = torch.rand(10, 64, 8, 8)
    encoder = model.EncoderRNN(64, 128, 2)
    output, *_ = encoder(convolution_result)
    assert output.shape[0] == 10
    assert output.shape[2] == 2


def test_decoder_forward() -> None:
    encoder_output = torch.rand(10, 64, 2)
    encoder_hidden = torch.rand(2, 10, 128)
    decoder = model.DecoderRNN(128, 128, 10)

    with patch("ocr.model.MAX_LEN", 3):
        output, *_ = decoder(encoder_output, (encoder_hidden, encoder_hidden))

    assert output.shape == (10, 3, 10)


def test_ocr_forward() -> None:
    img = torch.rand(1, 1, 100, 32)
    target_mock = torch.randint(1, 3, (1, 10))
    with patch("ocr.model.MAX_LEN", 3), patch("ocr.model.N_OUTPUT", 7):
        model_ = model.init_model()
        result, *_ = model_(img)
        assert result.shape == (1, 3, 7)
        result, *_ = model_(img, target_mock)
        assert result.shape == (1, 3, 7)
