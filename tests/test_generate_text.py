from ocr.generator import random_text


def test_generate_text():
    rt = random_text.generate_random_text(10)
    assert rt[0] not in " \n"
    assert rt[-1] not in " \n"
    assert len(rt) == 10


def test_generate_small():
    assert random_text.generate_random_text(-1) == ""
    assert random_text.generate_random_text(0) == ""
    assert len(random_text.generate_random_text(1)) == 1
