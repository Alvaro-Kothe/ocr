from ocr.generator import generate_image


def test_write_text() -> None:
    img = generate_image.text_to_image("Foo Baz Baz\n" * 2)
    start_pixel = img.getpixel((10, 10))
    different_colors = False
    for i in range(img.size[0]):
        for j in range(img.size[1]):
            if img.getpixel((i, j)) != start_pixel:
                different_colors = True
                break
        if different_colors:
            break
    assert different_colors, "All pixels are equal"
