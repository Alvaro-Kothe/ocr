import pathlib
import random
from collections.abc import Sequence
from os import PathLike

import matplotlib.pyplot as plt
import torch
from torch import Tensor, nn

from ocr.data.data_iterator import get_dataloader
from ocr.data.encode import PAD
from ocr.model import OCR, init_model

EARLY_STOP = 10_000
SAVE_PATH = pathlib.Path("./ocr.pt")


def train_batch(
    image: Tensor,
    target: Tensor,
    model: OCR,
    optimizer: torch.optim.Optimizer,
    criterion: nn.CrossEntropyLoss,
    teacher_forcing: bool,
) -> float:
    optimizer.zero_grad()
    teach_force_target = target if teacher_forcing else None
    logits, _ = model(image, teach_force_target)

    loss = criterion(logits.view(-1, logits.size(-1)), target.view(-1))

    loss.backward()

    optimizer.step()

    return loss.item()


def display_loss(losses: Sequence[float]) -> None:
    plt.figure()
    plt.plot(losses)
    plt.show()


def checkpoint(model: OCR, path: str | PathLike) -> None:
    torch.save(model.state_dict(), path)


def load_model(path: str | PathLike) -> OCR:
    model = init_model()
    try:
        model.load_state_dict(torch.load(path))
    except Exception as e:
        print(e)
    return model


def main() -> None:
    train_steps = 1_000_000
    print_every = 10
    teacher_force_prob = 0.05
    plot_every = 100
    plot_loss = 0.0
    print_loss = 0.0
    batch_size = 64
    losses: list[float] = []

    model = load_model(SAVE_PATH)
    model.train()

    optimizer = torch.optim.Adam(model.parameters())
    criterion = nn.CrossEntropyLoss(ignore_index=PAD)
    min_loss = float("inf")
    patience = EARLY_STOP

    data_loader = get_dataloader(batch_size)
    data_iter = iter(data_loader)

    for i in range(1, train_steps + 1):
        image, target = next(data_iter)
        use_teacher_forcing = random.random() < teacher_force_prob
        loss = train_batch(
            image, target, model, optimizer, criterion, use_teacher_forcing
        )
        if loss < min_loss:
            min_loss = loss
            patience = EARLY_STOP
            checkpoint(model, SAVE_PATH)
        else:
            patience -= 1
            if patience < 0:
                break

        plot_loss += loss
        print_loss += loss

        if i % print_every == 0:
            avg_loss = print_loss / print_every
            print_loss = 0.0
            print(
                f"{i} ({i/train_steps:.1%}) {loss:.4f} {patience}" f" ({avg_loss:.4f})"
            )

        if i % plot_every == 0:
            losses.append(plot_loss / plot_every)
            plot_loss = 0.0

    print("Saving model")
    torch.save(model.state_dict(), "ocr.pt")
    display_loss(losses)


if __name__ == "__main__":
    main()
