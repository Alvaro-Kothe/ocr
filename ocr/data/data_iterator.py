from collections.abc import Generator

import torch.utils.data
from torch.utils.data.dataloader import DataLoader

from ocr.data.encode import PAD
from ocr.data.generator import random_training_example


def collate_fn(batch):
    images, labels = zip(*batch)
    images = torch.stack(images, dim=0)

    labels = torch.nn.utils.rnn.pad_sequence(
        labels, batch_first=True, padding_value=PAD
    )

    return images, labels


class GeneratorDataset(torch.utils.data.IterableDataset):
    def __init__(self) -> None:
        super(GeneratorDataset, self).__init__()

    def __iter__(self) -> Generator[tuple[torch.Tensor, torch.Tensor], None, None]:
        while True:
            yield random_training_example()


def get_dataloader(batch_size: int) -> DataLoader:
    dataset = GeneratorDataset()
    data_loader = DataLoader(dataset, batch_size=batch_size, collate_fn=collate_fn)
    return data_loader
