import random

import torch
from torchvision.transforms import v2

from ocr.data.encode import CHAR_TO_INDEX, EOS
from ocr.generator import generate_image, random_text
from ocr.model import MAX_LEN

IMG_SIZE = 32

# TODO: Improve image transform pipeline
IMAGE_TRANSFORM_PIPELINE = v2.Compose(
    [
        v2.ToImage(),
        v2.Grayscale(),
        v2.Resize(size=(IMG_SIZE, IMG_SIZE), antialias=True),
        v2.ToDtype(torch.float32, scale=True),
        v2.Normalize((0.5,), (0.5,)),
    ]
)


def _target_to_tensor(s: str) -> torch.Tensor:
    """Transform a string into a tensor with EOS at the end.

    Args:
        s: Target string.

    Returns:
        Torch tensor of type long.
    """
    letter_indexes = [CHAR_TO_INDEX[c] for c in s] + [EOS]
    pad_size = (0, MAX_LEN - len(letter_indexes))
    result = torch.tensor(letter_indexes, dtype=torch.int64)
    return torch.nn.functional.pad(result, pad_size)


def random_training_example() -> tuple[torch.Tensor, torch.Tensor]:
    """Generate a random training example

    Generate a random text with variable length, creates an image from
    that text and convert both image and text into tensors.

    Returns:
        Image and text as tensors
    """
    # TODO: Generate random text size
    text_size = random.randint(0, 19)
    # text_size = 3
    text = random_text.generate_random_text(text_size)
    image = generate_image.text_to_image(text)
    image_tensor: torch.Tensor = IMAGE_TRANSFORM_PIPELINE(image)
    return image_tensor, _target_to_tensor(text)
