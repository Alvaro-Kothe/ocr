import string

PAD = 0
SOS = 1
EOS = 2

VALID_ALL_TIME_CHARACTERS = string.ascii_letters + string.digits + ".;'"
INVALID_START_AND_END = " "
ALL_CHARS = VALID_ALL_TIME_CHARACTERS + INVALID_START_AND_END


CHAR_TO_INDEX = {"<PAD>": PAD, "<EOS>": EOS, "<SOS>": SOS} | {
    c: i + EOS + 1 for i, c in enumerate(ALL_CHARS)
}
INDEX_TO_CHAR = {v: k for k, v in CHAR_TO_INDEX.items()}
N_OUTPUT = len(CHAR_TO_INDEX)
