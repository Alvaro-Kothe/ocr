import random

from ocr.data import encode


def generate_random_text(text_len: int = 20):
    """Generate random string

    Args:
        text_len: Length of the generated text

    Returns:
        Random string
    """
    if text_len <= 0:
        return ""

    result = random.choice(encode.VALID_ALL_TIME_CHARACTERS)
    i = 1
    while len(result) < text_len:
        if i == text_len - 1:
            result += random.choice(encode.VALID_ALL_TIME_CHARACTERS)
        else:
            result += random.choice(encode.ALL_CHARS)
        i += 1
    return result
