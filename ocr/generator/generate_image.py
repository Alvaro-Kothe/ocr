"""
Generate png images from a python string.
"""

import numpy as np
import numpy.typing as npt
from PIL import Image, ImageDraw, ImageFont


def text_to_image(text: str) -> Image.Image:
    """Generate image from an string

    Args:
        text: Text to be printed in the image.

    Returns:
        Image with printed text.
    """
    image = Image.new("RGB", (100, 32), color=(255, 255, 255))
    font = ImageFont.load_default()
    draw = ImageDraw.Draw(image)
    draw.multiline_text((10, 10), text, font=font, fill=(0, 0, 0))
    return image


def text_to_array(text: str) -> npt.NDArray[np.uint8]:
    """Generate an image 3D array

    Generate an array from a text. The array values will be integers from 0-255.

    Args:
        text: Text to be printed and put in the array.

    Returns:
        Numpy array.
    """
    img = text_to_image(text)
    return np.asarray(img, dtype=np.uint8)
