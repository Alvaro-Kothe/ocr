import torch
from torch import nn

from ocr.data.encode import N_OUTPUT, SOS

MAX_LEN = 20


class ConvolutionSeq(nn.Module):
    def __init__(self, in_channels: int, out_channels: int) -> None:
        super(ConvolutionSeq, self).__init__()
        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size=3, padding=1)
        self.relu = nn.ReLU()
        self.pool = nn.MaxPool2d(kernel_size=2, stride=2)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        return self.pool(self.relu(self.conv(x)))


class ConvolutionLayer(nn.Module):
    def __init__(self) -> None:
        super(ConvolutionLayer, self).__init__()
        self.conv1 = ConvolutionSeq(1, 32)
        self.conv2 = ConvolutionSeq(32, 64)
        self.conv3 = ConvolutionSeq(64, 128)
        self.conv4 = ConvolutionSeq(128, 256)
        self.conv5 = ConvolutionSeq(256, 512)
        self.batchnorm = nn.BatchNorm2d(512)

    def forward(self, x):
        # Input shape (batch size, channels, height, width) channels = 1
        x = self.conv1(x)
        x = self.conv2(x)
        x = self.conv3(x)
        x = self.conv4(x)
        x = self.conv5(x)
        x = self.batchnorm(x)
        return x


class EncoderRNN(nn.Module):
    def __init__(self, input_size: int, hidden_size: int, output_size: int):
        super(EncoderRNN, self).__init__()
        self.rnn = nn.LSTM(
            input_size, hidden_size, batch_first=True, bidirectional=True
        )
        self.linear = nn.Linear(hidden_size * 2, output_size)

    def forward(
        self, convoluted_image: torch.Tensor
    ) -> tuple[torch.Tensor, torch.Tensor]:
        batch_size, channels, height, width = convoluted_image.size()
        x = convoluted_image.view(
            batch_size, height * width, channels
        )  # (b, seq_len, input_size)
        output, hidden = self.rnn(x)
        output = self.linear(output)
        return output, hidden


class DecoderRNN(nn.Module):
    def __init__(self, input_size: int, hidden_size: int, output_size: int):
        super(DecoderRNN, self).__init__()
        self.embedding = nn.Embedding(output_size, input_size)
        self.rnn = nn.LSTM(
            input_size, hidden_size, batch_first=True, bidirectional=True
        )
        self.fc = nn.Linear(2 * hidden_size, output_size)
        self.relu = nn.ReLU()

    def forward(
        self,
        encoder_outputs: torch.Tensor,
        encoder_hidden: torch.Tensor,
        target_tensor: torch.Tensor | None = None,
    ) -> tuple[torch.Tensor, torch.Tensor]:
        batch_size = encoder_outputs.size(0)
        decoder_input = torch.full((batch_size, 1), SOS, dtype=torch.long)
        decoder_hidden = encoder_hidden
        outputs: list[torch.Tensor] = []

        for i in range(MAX_LEN):
            logits, decoder_hidden = self.forward_step(decoder_input, decoder_hidden)
            outputs.append(logits)

            if target_tensor is None:
                # choose most probable next token from last dimension
                argmax_ = logits.argmax(-1, keepdim=True)
                decoder_input = argmax_.squeeze(-1).detach()
            else:
                decoder_input = target_tensor[:, i].unsqueeze(1)  # Teacher forcing

        outputs_logits = torch.cat(outputs, dim=1)

        return outputs_logits, decoder_hidden

    def forward_step(
        self, context: torch.Tensor, hidden: torch.Tensor
    ) -> tuple[torch.Tensor, torch.Tensor]:
        x = self.embedding(context)
        x = self.relu(x)
        x, hidden = self.rnn(x, hidden)
        output = self.fc(x)
        return output, hidden


class OCR(nn.Module):
    def __init__(self, hidden_size: int, output_size: int) -> None:
        super(OCR, self).__init__()
        self.conv = ConvolutionLayer()
        self.encoder = EncoderRNN(512, hidden_size, hidden_size)
        self.decoder = DecoderRNN(hidden_size, hidden_size, output_size)

    def forward(
        self, image: torch.Tensor, target_tensor: torch.Tensor | None = None
    ) -> tuple[torch.Tensor, torch.Tensor]:
        convolved_image = self.conv(image)
        encoder_output, encoder_hidden = self.encoder(convolved_image)
        logits, hidden = self.decoder(encoder_output, encoder_hidden, target_tensor)
        return logits, hidden


def init_model() -> OCR:
    return OCR(256, N_OUTPUT)
