import sys

import torch
from PIL import Image

from ocr.data.encode import EOS, INDEX_TO_CHAR
from ocr.data.generator import IMAGE_TRANSFORM_PIPELINE
from ocr.train import load_model, SAVE_PATH

MAX_LEN = 20

# TODO: Improve model load
model = load_model(SAVE_PATH)
model.eval()


def predict(image_tensor: torch.Tensor) -> str:
    result = ""
    with torch.no_grad():
        output, _ = model(image_tensor)
        ids = output.argmax(-1).squeeze()

        for idx in ids:
            idx = idx.item()
            if idx == EOS:
                return result

            result += INDEX_TO_CHAR[idx]

    return result


def read_png_from_stdin() -> Image.Image:
    return Image.open(sys.stdin.buffer)


def main():
    img = read_png_from_stdin()
    # img = Image.open("./test.png")
    img_tensor: torch.Tensor = IMAGE_TRANSFORM_PIPELINE(img)
    result = predict(img_tensor.unsqueeze(0))
    print(result)


if __name__ == "__main__":
    main()
